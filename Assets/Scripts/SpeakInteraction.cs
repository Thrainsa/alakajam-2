﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SpeakInteraction : Interactable {

    public SpeakInteractionData interaction;

    private AudioSource audioSource;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        audioSource = GetComponent<AudioSource>();
        if(LayerMask.LayerToName(gameObject.layer) != "Interactable")
        {
            Debug.LogError("This interactable object: "+gameObject.name+" doesn't have the right layer: 'Interactable'");
        }
    }

    public override void Interact()
    {
        subtitleController.ResetSubtitles();
        for (int i = 0; i < interaction.text.Length; i++)
        {
            subtitleController.AddSubtitle(interaction.text[i], interaction.textDuration[i]);
        }
        foreach (SpeakInteraction speakInteraction in FindObjectsOfType<SpeakInteraction>())
        {
            AudioSource source = speakInteraction.GetComponent<AudioSource>();
            if (source != null && speakInteraction.interaction.audioClip != null)
            {
                source.Stop();
            }
            audioSource.PlayOneShot(interaction.audioClip);
        }
    }
}
