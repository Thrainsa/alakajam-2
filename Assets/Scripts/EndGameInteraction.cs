﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

[RequireComponent(typeof(AudioSource))]
public class EndGameInteraction : SpeakInteraction {

    public Animator swordAnimator;
    public Animator playerAnimator;
    public GameObject player;
    private GameManager gameManager;

    protected override void Start()
    {
        base.Start();
        gameManager = FindObjectOfType<GameManager>();
    }

    public override void Interact()
    {
        player.GetComponent<FirstPersonController>().enabled = false;
        gameManager.CheckPoint(1);
        gameManager.Death();


        StartCoroutine(EndAnimation());

    }

    private IEnumerator EndAnimation()
    {
        yield return new WaitForSeconds(1);
        base.Interact();
        yield return new WaitForSeconds(5);
        swordAnimator.SetTrigger("start");
        yield return new WaitForSeconds(10);

        swordAnimator.SetTrigger("start");
        playerAnimator.enabled = true;
        playerAnimator.SetTrigger("start");

        yield return new WaitForSeconds(2);

        subtitleController.AddSubtitle("Press escape to Quit the game", -1);
    }
}
