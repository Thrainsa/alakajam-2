﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTextureSetup : MonoBehaviour {

    public Camera cameraA;
    public Material cameraMatA;

    public Camera cameraB;
    public Material cameraMatB;


    // Use this for initialization
    void Start ()
    {
        InitCamera(cameraA, cameraMatA);
        InitCamera(cameraB, cameraMatB);
    }

    private void InitCamera(Camera camera, Material cameraMat)
    {
        if (camera.targetTexture != null)
        {
            camera.targetTexture.Release();
        }

        camera.targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
        cameraMat.mainTexture = camera.targetTexture;
    }
}
