﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class NewScenePortalTeleporter : PortalTeleporter {

    public string sceneName;
    public int sceneReceiver;

    // Update is called once per frame
    void Update () {
        if (playerIsOverlapping)
        {
            SceneManager.LoadScene(sceneName);
        }
	}
}
