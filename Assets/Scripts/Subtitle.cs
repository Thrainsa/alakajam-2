﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public class Subtitle {

    public string text;
    public float duration;

    public Subtitle(string text, float duration)
    {
        this.text = text;
        this.duration = duration;
    }
}
