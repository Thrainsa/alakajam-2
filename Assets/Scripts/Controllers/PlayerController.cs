﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour {

    public LayerMask interactableLayerMask;

    private int interactionRange = 4;
    private Camera fpsCam;
    private GameManager gameManager;

    private Interactable interactable;

    // Use this for initialization
    void Start () {
        fpsCam = GetComponentInChildren<Camera>();
        gameManager = FindObjectOfType<GameManager>();
    }

	// Update is called once per frame
	void Update () {
        if (CrossPlatformInputManager.GetButtonDown("Interact") && this.interactable != null)
        {
            this.interactable.Interact();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    void FixedUpdate()
    {
        RaycastHit hit;

        // Create a vector at the center of our camera's viewport
        Vector3 rayOrigin = fpsCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0.0f));

        Debug.DrawLine(transform.position, rayOrigin + fpsCam.transform.forward * interactionRange, Color.cyan);

        if (Physics.Raycast(rayOrigin, fpsCam.transform.forward, out hit, interactionRange, interactableLayerMask))
        {
            Debug.DrawLine(transform.position, rayOrigin + fpsCam.transform.forward * interactionRange, Color.red);
            this.interactable = hit.transform.gameObject.GetComponent<Interactable>();
            this.interactable.ShowInfo();
        }
        else
        {
            if(this.interactable != null)
            {
                this.interactable.HideInfo();
            }
            this.interactable = null;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Death")
        {
            gameManager.Death();
        }
    }
}
