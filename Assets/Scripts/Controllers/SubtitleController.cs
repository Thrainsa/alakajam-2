﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SubtitleController : MonoBehaviour {

    public GameObject subtitlePanel;
    public TextMeshProUGUI subtitleText;

    public GameObject infoPanel;
    public TextMeshProUGUI infoText;

    public List<Subtitle> subtitles = new List<Subtitle>();

    public float showNext = -1;

    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (subtitlePanel.activeSelf && showNext != -1 && Time.time > showNext)
        {
            if (this.subtitles.Count == 0)
            {
                HideSubtitle();
            }
            else
            {
                ShowNextSubtitle();
            }
        }
        else if (!subtitlePanel.activeSelf && this.subtitles.Count > 0)
        {
            ShowNextSubtitle();
        }
    }

    private void HideSubtitle()
    {
        subtitlePanel.SetActive(false);
    }

    private void ShowNextSubtitle() { 
        subtitlePanel.SetActive(false);
        subtitlePanel.SetActive(true);
        Subtitle subtitle = this.subtitles[0];
        this.subtitles.RemoveAt(0);
        subtitleText.SetText(subtitle.text);
        if (subtitle.duration != -1)
        {
            showNext = Time.time + subtitle.duration;
        }
        else
        {
            showNext = -1;
        }

        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)subtitleText.transform);
    }

    public void NextSubtitle()
    {
        showNext = 0;
    }

    public void ResetSubtitles()
    {
        this.subtitles.Clear();
        NextSubtitle();
    }

    public void AddSubtitles(List<Subtitle> newSubtitles)
    {
        subtitles.AddRange(newSubtitles);
    }

    public void AddSubtitle(Subtitle subtitle)
    {
        subtitles.Add(subtitle);
    }

    public void AddSubtitle(string text, float duration)
    {
        AddSubtitle(new Subtitle(text, duration));
    }

    // Info Panel part
    public void ShowInfoMessage(string message)
    {
        infoPanel.SetActive(true);
        infoText.SetText(message);
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)infoText.transform);
    }

    public void HideInfoMessage()
    {
        infoPanel.SetActive(false);
    }

}
