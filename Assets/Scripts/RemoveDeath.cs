﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveDeath : MonoBehaviour {

    [SerializeField]
    public GameObject[] deathObjectsToClear;
    [SerializeField]
    public GameObject[] objectsToDelete;

    // Update is called once per frame
    void Update () {
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            foreach (GameObject target in deathObjectsToClear)
            {
                target.tag = "Untagged";
            }
            foreach (GameObject target in objectsToDelete)
            {
                Destroy(target);
            }

        }
    }

}
