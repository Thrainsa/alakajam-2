﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class GameManager : MonoBehaviour
{
    public GameObject gameOverPanel;
    public GameObject player;

    private SubtitleController subtitleController;
    public PlayerStart[] startPositions;

    private int startPositionIndex = 0;

    // Use this for initialization
    void Start()
    {
        subtitleController = FindObjectOfType<SubtitleController>();
        startPositions = FindObjectsOfType<PlayerStart>();
        ResetPlayerPosition();
        gameOverPanel.GetComponent<Image>().color = new Color(0f, 0f, 0f, 1f);
       StartCoroutine(FadeOut());
    }

    private void ResetPlayerPosition()
    {
        foreach (PlayerStart playerStart in startPositions)
        {
            if (playerStart.index == startPositionIndex)
            {
                player.transform.position = playerStart.transform.position;
                player.transform.rotation = playerStart.transform.rotation;
                player.GetComponentInChildren<Camera>().transform.rotation = Quaternion.identity;
                break;
            }
        }
    }



    IEnumerator FadeIn()
    {
        gameOverPanel.SetActive(true);
        float startTime = Time.time;
        float duration = 0.5f;
        while (gameOverPanel.GetComponent<Image>().color.a < 1.0f)
        {
            gameOverPanel.GetComponent<Image>().color = new Color(0f, 0f, 0f, Mathf.Lerp(0f, 1f, (Time.time - startTime) / duration));
            yield return null;
        }

        ResetPlayerPosition();

        StartCoroutine(FadeOut());
    }

    IEnumerator FadeOut()
    {
        gameOverPanel.SetActive(true);
        float startTime = Time.time;
        float duration = 1f;
        while (gameOverPanel.GetComponent<Image>().color.a > 0f)
        {
            gameOverPanel.GetComponent<Image>().color = new Color(0f, 0f, 0f, Mathf.Lerp(1f, 0f, (Time.time - startTime) / duration));
            yield return null;
        }

        gameOverPanel.SetActive(false);
    }

    public void Death()
    {
        StartCoroutine(FadeIn());
        subtitleController.ResetSubtitles();
    }

    public void CheckPoint(int newPlayerStartPosition)
    {
        this.startPositionIndex = newPlayerStartPosition;
    }
}
