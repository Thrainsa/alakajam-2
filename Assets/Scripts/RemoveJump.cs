﻿using System.Reflection;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class RemoveJump : MonoBehaviour {

    private float oldSpeed;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            FirstPersonController firstPersonController = other.GetComponent<FirstPersonController>();
            FieldInfo prop = GetJumpSpeedFied(firstPersonController);

            oldSpeed = (float)prop.GetValue(firstPersonController);
            prop.SetValue(firstPersonController, 0f);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            FirstPersonController firstPersonController = other.GetComponent<FirstPersonController>();

            FieldInfo prop = GetJumpSpeedFied(firstPersonController);
            prop.SetValue(firstPersonController, oldSpeed);
        }
    }

    private static FieldInfo GetJumpSpeedFied(FirstPersonController firstPersonController)
    {
        return firstPersonController.GetType().GetField("m_JumpSpeed", BindingFlags.NonPublic | BindingFlags.Instance);
    }

}
