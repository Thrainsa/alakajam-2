﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : MonoBehaviour {

    protected SubtitleController subtitleController;

    public abstract void Interact();

    protected virtual void Start()
    {
        subtitleController = FindObjectOfType<SubtitleController>();
    }

    public void ShowInfo()
    {
        subtitleController.ShowInfoMessage("Press <b>E</b> to interact");
    }

    public void HideInfo()
    {
        subtitleController.HideInfoMessage();
    }
}
