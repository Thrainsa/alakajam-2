﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoInteraction : MonoBehaviour {

    public Interactable autoInteractOnce;

    public bool interactedOnce = false;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !interactedOnce)
        {
            autoInteractOnce.Interact();
            interactedOnce = true;
        }
    }
}
