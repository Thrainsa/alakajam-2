﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Interactable", order = 1)]
public class SpeakInteractionData : ScriptableObject
{
    public string objectName = "New SpeakInteractionData";
    public string[] text;
    public float[] textDuration;
    public AudioClip audioClip;
}